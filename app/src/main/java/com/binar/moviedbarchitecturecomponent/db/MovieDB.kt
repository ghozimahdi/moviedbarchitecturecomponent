package com.binar.moviedbarchitecturecomponent.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.binar.moviedbarchitecturecomponent.db.dao.MovieDao
import com.binar.moviedbarchitecturecomponent.db.entities.Movie

@Database(entities = [Movie::class], version = 1)
abstract class MovieDB : RoomDatabase() {
    abstract fun movieDao() : MovieDao

    companion object {
        var INSTANCE: MovieDB? = null

        fun getAppDataBase(context: Context): MovieDB {
            if (INSTANCE == null){
                synchronized(MovieDB::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, MovieDB::class.java, "myDB").build()
                }
            }
            return INSTANCE!!
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}
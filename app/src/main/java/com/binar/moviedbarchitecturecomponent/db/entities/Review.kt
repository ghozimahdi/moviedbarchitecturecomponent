package com.binar.moviedbarchitecturecomponent.db.entities

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Review {
    @SerializedName("author")
    @Expose
    private var author: String? = null

    @SerializedName("content")
    @Expose
    private var content: String? = null

    @SerializedName("id")
    @Expose
    private var id: String? = null

    @SerializedName("url")
    @Expose
    private var url: String? = null

    constructor(author: String?, content: String?) {
        this.author = author
        this.content = content
    }


    fun getAuthor(): String? {
        return author
    }

    fun setAuthor(author: String?) {
        this.author = author
    }

    fun getContent(): String? {
        return content
    }

    fun setContent(content: String?) {
        this.content = content
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url
    }
}
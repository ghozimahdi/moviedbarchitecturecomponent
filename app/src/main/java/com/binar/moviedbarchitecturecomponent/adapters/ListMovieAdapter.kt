package com.binar.moviedbarchitecturecomponent.adapters

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop

class ListMovieAdapter: RecyclerView.Adapter<ListMovieAdapter.ListMovieHolder>() {
    private var onItemClickCallback: ListMovieAdapter.OnItemClickCallback? = null
    private var listItem: List<Movie> = ArrayList()
    inner class ListMovieHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        fun bind(item: Movie) {
            val ivMoviePoster= itemView.findViewById<ImageView>(R.id.ivMoviePoster)
            with(itemView) {
                Glide.with(itemView.context)
                    .load("https://image.tmdb.org/t/p/w342${item.getPosterPath()}")
                    .transform(CenterCrop())
                    .into(ivMoviePoster)
            }

            val tvMovieTitle= itemView.findViewById<TextView>(R.id.tvMovieTitle)
            val tvMovieYear= itemView.findViewById<TextView>(R.id.tvMovieRating)

            tvMovieTitle.text = item.getTitle()
            tvMovieYear.text = item.getVoteAverage().toString()
            itemView.setOnClickListener { onItemClickCallback?.onItemClicked(item) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMovieHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return ListMovieHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ListMovieHolder, position: Int) {
        holder.bind(listItem[position])
    }

    fun setItem(listItem: List<Movie>){
        if (listItem != null) {
            Log.d(ContentValues.TAG, "setItem")
            this.listItem = listItem
        }
    }
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
    interface OnItemClickCallback {
        fun onItemClicked(data: Movie)
    }
}
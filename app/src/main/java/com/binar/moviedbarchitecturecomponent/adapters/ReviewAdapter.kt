package com.binar.moviedbarchitecturecomponent.adapters

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.db.entities.Review

class ReviewAdapter: RecyclerView.Adapter<ReviewAdapter.ReviewHolder>() {
    private var onItemClickCallback: ReviewAdapter.OnItemClickCallback? = null
    private var listItem: List<Review> = ArrayList()
    inner class ReviewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)   {
        fun bind(item: Review) {
            val tvNameUser = itemView.findViewById<TextView>(R.id.tvNameReview)
            val tvDescription = itemView.findViewById<TextView>(R.id.tvDescriptionReview)

            tvNameUser.text = item.getAuthor()
            tvDescription.text = item.getContent()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ReviewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_review, parent, false)
        return ReviewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ReviewAdapter.ReviewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    fun setItem(listItem: List<Review>){
        if (listItem != null) {
            Log.d(ContentValues.TAG, "setItem")
            this.listItem = listItem
        }
    }
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
    interface OnItemClickCallback {
        fun onItemClicked(data: Review)
    }
}
package com.binar.moviedbarchitecturecomponent.adapters

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.db.entities.Genre
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import kotlinx.android.synthetic.main.item_genre.view.*

class GenreAdapter : RecyclerView.Adapter<GenreAdapter.GenreHolder>(){
    private var onItemClickCallback: OnItemClickCallback? = null
    private var listItem: List<Genre> = ArrayList()
    inner class GenreHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Genre) {
            val tvGenreName= itemView.findViewById<TextView>(R.id.tvGenreName)
            with(itemView) {
                Glide.with(itemView.context)
                    .load("https://image.tmdb.org/t/p/w342${item.getBackdropPath()}")
                    .transform(CenterCrop())
                    .into(ivBGGenre)
            }
            Log.d(TAG, "bind: ${item.getBackdropPath()}")
            tvGenreName.text = item.getName()
            itemView.setOnClickListener { onItemClickCallback?.onItemClicked(item) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreAdapter.GenreHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_genre, parent, false)
        return GenreHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: GenreAdapter.GenreHolder, position: Int) {
        holder.bind(listItem[position])
    }

    fun setItem(listItem: List<Genre>){
        if (listItem != null) {
            Log.d(TAG, "setItem")
            this.listItem = listItem
        }
    }
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Genre)
    }

}
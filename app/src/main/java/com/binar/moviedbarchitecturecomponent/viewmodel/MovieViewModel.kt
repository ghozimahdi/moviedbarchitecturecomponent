package com.binar.moviedbarchitecturecomponent.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.binar.moviedbarchitecturecomponent.repository.MovieRepository

class MovieViewModel(private val repository: MovieRepository): ViewModel() {
    fun fetchMovieData(page:Int, genre:String, context: Context?) = repository.fetchMoviesData(page, genre, context)
    fun getTrending(context: Context?) = repository.getTrending(context)
    fun getMovieData() = repository.getAllMovies()
    fun getMovieTrailer(movieId: Int, context: Context?) = repository.getMovieTrailer(movieId, context)
    fun getMovieReviews(movieId:Int, context: Context?) = repository.getMovieReviews(movieId, context)
    fun getGenre(context: Context?) = repository.getGenre(context)
}
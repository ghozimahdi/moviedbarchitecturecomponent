package com.binar.moviedbarchitecturecomponent.rest

import com.binar.moviedbarchitecturecomponent.db.entities.Genre
import com.google.gson.annotations.SerializedName

data class GetGenreResponse(
    @SerializedName("genres")
    val genre: List<Genre>
)
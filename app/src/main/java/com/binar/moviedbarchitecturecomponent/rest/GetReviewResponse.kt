package com.binar.moviedbarchitecturecomponent.rest

import com.binar.moviedbarchitecturecomponent.db.entities.Review
import com.google.gson.annotations.SerializedName


data class GetReviewResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<Review>
)
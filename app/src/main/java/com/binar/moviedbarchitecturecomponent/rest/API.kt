package com.binar.moviedbarchitecturecomponent.rest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface API {

    @GET("trending/movie/day")
    fun getTrending(
        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a"
    ): Call<GetMoviesResponse>


    @GET("discover/movie")
    fun setMovies(
        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a",
        @Query("page") page: Int,
        @Query("with_genres") genre: String
    ): Call<GetMoviesResponse>

    @GET("movie/{movie_id}/videos")
    fun getTrailer(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a"

    ): Call<GetTrailerResponse>

    @GET("movie/{movie_id}/reviews")
    fun getReview(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a"

    ): Call<GetReviewResponse>

    @GET("genre/movie/list")
    fun getGenre(
        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a"
    ): Call<GetGenreResponse>
}
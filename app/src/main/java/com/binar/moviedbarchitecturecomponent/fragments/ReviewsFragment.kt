package com.binar.moviedbarchitecturecomponent.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.adapters.ReviewAdapter
import com.binar.moviedbarchitecturecomponent.db.entities.Review
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.fragment_reviews.*
import org.koin.android.ext.android.inject


class ReviewsFragment : Fragment() {
    private lateinit var adapter: ReviewAdapter

    private val movieViewModel: MovieViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reviews, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ReviewAdapter()
        adapter.notifyDataSetChanged()
        if (arguments !=null)
        {
            val movieId = arguments!!.getInt("movieId")
            fetchData(movieId)
        }

    }

    private fun fetchData(movieId:Int) {
        movieViewModel.getMovieReviews(movieId, context).observe(viewLifecycleOwner, Observer<List<Review>> { items->
            if (items != null){
                setupRecyclerView(items)
            }
        })
    }

    private fun setupRecyclerView(items: List<Review>) {
        adapter.setItem(items)
        adapter.setOnItemClickCallback(object : ReviewAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Review) {
                selectReview(data)
            }
        })

        rvAllReview.layoutManager = LinearLayoutManager(activity)
        rvAllReview.setHasFixedSize(true)
        rvAllReview.isNestedScrollingEnabled = false
        rvAllReview.adapter = adapter
    }

    private fun selectReview(data: Review) {
        Toast.makeText(activity, data.getAuthor(), Toast.LENGTH_SHORT).show()
    }
}
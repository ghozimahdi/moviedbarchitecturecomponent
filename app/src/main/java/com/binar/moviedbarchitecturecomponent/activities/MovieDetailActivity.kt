package com.binar.moviedbarchitecturecomponent.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.binar.moviedbarchitecturecomponent.fragments.DetailMovieFragment
import com.binar.moviedbarchitecturecomponent.fragments.ReviewsFragment

class MovieDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        if (intent.extras != null) {
            val bundle = intent.extras
            val movie = bundle?.getSerializable("movie") as Movie
            showDetailMovie(movie)
        }
    }

    private fun showDetailMovie(data: Movie){
        val detailMovieFragment: Fragment = DetailMovieFragment()
        val bundle = Bundle()
        bundle.putSerializable("selectedMovie", data)
        detailMovieFragment.arguments = bundle
        val ft: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameMovie, detailMovieFragment).commit()
    }

    fun showAllReview(id: Int?) {
        val reviewsFragment: Fragment = ReviewsFragment()
        val bundle = Bundle()
        if (id != null) {
            bundle.putInt("movieId", id)
        }
        reviewsFragment.arguments = bundle
        val ft: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameMovie, reviewsFragment).addToBackStack("AllReview").commit()
    }


}